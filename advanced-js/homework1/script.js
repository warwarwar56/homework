class Employee {
    constructor (name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this.name;
    }
    set name(value) {
        this.name = value;
    }
    get age() {
        return this.age;
    }
    set age(value) {
        this.age = value;
    }
    get salary() {
        return this.salary;
    }
    set salary(value) {
        this.salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this._lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
}

let programmer1 = new Programmer('Vasya', 19, 3000, 'Eng');
let programmer2 = new Programmer('Nikita', 22, 4000, 'Eng');
let programmer3 = new Programmer('Kolya', 40, 1000, 'Eng');

console.log(programmer1);
console.log(programmer2.salary);
console.log(programmer3.salary)
