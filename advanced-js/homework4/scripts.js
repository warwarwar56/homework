const results = document.querySelector('#results');
const filmsLink = 'https://ajax.test-danit.com/api/swapi/films'


function sendRequest(url) {
    return fetch(url).then(response => response.json())
}

function renderFilms(filmsList) {
    filmsList.forEach(film => {
        const ul = document.createElement('ul');
        const characters = document.createElement('ul');
        characters.className = 'charactersList';
        characters.innerText = 'characters:'
        for (prop in film) {
            if(prop === 'characters') {
                continue
            }
            const li = document.createElement('li');
            li.innerText = `${prop} : ${film[prop]}`
            ul.append(li);   
        } 
        ul.append(characters);
        results.append(ul);
    })
}



const info = sendRequest(filmsLink)
.then(films => films.map((film) => {
    return {
    filmTitle: film.name,
    episodeId: film.episodeId,
    opening: film.openingCrawl,
    characters: film.characters,
    }
}))
.then(films => {
    renderFilms(films)
    return films.map(film => film.characters)
})
.then(characters => {
    const lists = document.querySelectorAll('.charactersList');
    console.log(lists)
    characters.forEach((characterList, index) => {
        const renderedCharacters = document.createElement('ul');
        characterList.forEach(characterLink => {
            sendRequest(characterLink)
            .then(character => character.name)
            .then(name => {
                const li = document.createElement('li');
                li.innerText = name;
                renderedCharacters.append(li);

            })
            
        })
        lists[index].append(renderedCharacters)
    })
});
