const findButton = document.querySelector('#findById');
const reuslts = document.querySelector('#results');

function clearResults() {
    reuslts.innerHTML = '';

}

function renderAddress(address) {
    clearResults();
    const ul =  document.createElement('ul');
    for (prop in address) {
        
        const li = document.createElement('li');
        li.innerText = `${prop}: ${address[prop]}`;
        ul.append(li)
    }
    reuslts.append(ul);
}

async function getIp() {
    const getIp = 'https://api.ipify.org/?format=json';
    const ipResponse = await fetch(getIp);
    const { ip } = await ipResponse.json();
    return ip;
}

async function getAddress(ip) {
    const getAddress = 'http://ip-api.com/json/'
    const addressResponse = await fetch(`${getAddress}${ip}?fields=continent,country,regionName,city,district`);
    const address = await addressResponse.json();
    return address;

}


async function findById() {
    const ip = await getIp();
    const address = await getAddress(ip);
    renderAddress(address);

}

findButton.addEventListener('click', findById)
