import * as React from 'react';
import Input from '@mui/material/Input';
import './Inputs.css'
import TextField from '@mui/material/TextField';

const ariaLabel = { 'aria-label': 'description' };

export default function Inputs() {
  return (
    <div className='InputGroup'>
      <p><b>Зв'язатися з нами</b></p>
      <Input className='inp' data='name'placeholder="Ім'я" inputProps={ariaLabel} />
      <Input className='inp' data='email'placeholder="Email" inputProps={ariaLabel} />
      <Input  className='inp' data='phoneNumber'placeholder="Телефон" inputProps={ariaLabel} type="number" />
      <TextField label='Коментар'/>
    </div>
  );
}