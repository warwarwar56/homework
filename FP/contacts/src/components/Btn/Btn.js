import React from "react";
import Button from '@mui/material/Button';
import './Btn.css';


const Btn = () => {
  return <Button className='btn' variant="contained">Hello World</Button>;
}


export default Btn;
