import * as React from 'react';
import { Map, GoogleApiWrapper } from 'google-maps-react';

const MapG = () => {
  return (
    <Map
      google={this.props.google}
      zoom={15}
      initialCenter={{ lat: 9.761927, lng: 79.95244 }}
    />
  );
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyA0KBFxH8S8v1bHjD38azPx3KCMoZDI3x8'
})(MapG);