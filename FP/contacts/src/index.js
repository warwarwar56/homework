import * as React from 'react';
import ReactDOM from 'react-dom';
import  Btn  from './components/Btn/Btn';
import './index.css';
import Map from "./components/GoogleMap/Map";
import Inputs from './components/Inputs/Inputs'
import Description from './components/Description/Description';



const App = () => {
  
  return (
    <div className="App">
      <div className="sect1 width">
        <Description />
      </div>
      <div className="sect2 width">
        <Inputs />
        <Btn />
      </div>
      <div className="sect3 width">
      <Map
        google={this.props.google}
        zoom={15}
        initialCenter={{ lat: 9.761927, lng: 79.95244 }}
      />
      </div>
      
      
    </div>
  );
};


ReactDOM.render(<App />,
  document.getElementById('root')
);
