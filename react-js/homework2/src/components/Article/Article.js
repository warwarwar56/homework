import React from 'react'
import Button from '../Button/Button'
import Modal from '../Modal/Modal'
import Star from '../Star/Star'
import './Article.scss'


class Article extends React.Component {

    render() {
        const {article, index, addToFavorite, favorite, openModal, closeModal, addToCart, cart} = this.props;
        const {name, price, image, gameClass} = article
        




        return (
            <div class='article'>
                <p class='article__name'>{name}  <Star color='orangered' isFavorite={favorite.includes(`${index}`)} onClick={() => addToFavorite(favorite, index)}/></p>
                
                <img class='article__card' src={image} alt='card'/>
                <p class='article__game-class'>Класс: {gameClass}</p>
                <p class='article__price'>Цена: {price} <img class='article__price-icon' alt='arcane dust' src='/imgs/arcaneDust.png'/></p>
                <Button  text='добавить в корзину' onClick={() => openModal(<Modal header='Корзина' text='вы точно хотите добавить карту в корзину ?' closeModal={closeModal}
                actions={[<Button text='закрыть' onClick={closeModal}/>, <Button text='добавить в корзину' onClick={()=> {addToCart(cart, index); closeModal()}}/>]}/>)}/>
                
            </div>
        )
    }
}


export default Article;