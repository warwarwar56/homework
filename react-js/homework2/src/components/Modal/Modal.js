import React from 'react'
import Button from '../Button/Button'
import './Modal.scss'


class Modal extends React.Component {


    render() {
        const {header, closeButton, text, actions, closeModal} = this.props;
        
        

        return (
            <div className='modal' onClick={closeModal}>
                <div className='modal__container' onClick={(e) => e.stopPropagation()}>
                    <header className='modal__header'>  
                        {header}         
                        {closeButton && <Button className='closeButton' text={'x'} onClick={closeModal}/>}
                    </header>
                    <div className='modal__content'>{text}</div>
                    <footer className='modal__footer'>{actions}</footer>
                   
                </div>
            </div>
            
        )
    }
}


export default Modal;