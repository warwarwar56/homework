import React from 'react';
import './App.css';
import Articles from './components/Articles/Articles';

class App extends React.Component {
  state = {
    modal:null,
    articles: [],
    favorite: Array.from(new Set(localStorage.getItem('favorite'))),
    cart: Array.from(new Set(localStorage.getItem('cart')))
  }

  openModal = (modal) => {
    this.setState({modal});
  }

  closeModal = () => {
    this.setState({modal: null})
  }

  setArticles = (articles) => {
    this.setState({articles: articles})
  }

  toggleFavorite = (favorite, key) => {
    if (favorite.includes(`${key}`)) {
      this.setState({favorite: favorite.filter(index => index !== `${key}`)}, () => localStorage.setItem('favorite', this.state.favorite))
    }
    else this.setState({favorite: [...favorite, `${key}`]}, () => localStorage.setItem('favorite', this.state.favorite))
}

addToCart = (cart, key) =>  {
  // if (cart.includes(`${key}`)) {
  //   this.setState({cart: cart.filter(index => index !== `${key}`)})
  // }
  // else
  this.setState({cart: [...cart, `${key}`]}, () => localStorage.setItem('cart', this.state.cart))
}

    




 render() {
   return (
    <div className="App">
    <Articles 
    articles={this.state.articles} 
    setArticles={this.setArticles} 
    addToFavorite={this.toggleFavorite}
    favorite={this.state.favorite}
    openModal={this.openModal} 
    closeModal={this.closeModal}
    addToCart={this.addToCart}
    cart={this.state.cart}/>
    {this.state.modal}
    </div>
  );
} 
}

export default App;
