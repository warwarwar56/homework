import React, { useEffect, useState } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Articles from "../components/Articles/Articles";
import Favorite from "../components/Favorite/Favorite";
import Cart from "../components/Cart/Cart"

const AppRoutes = ({ openModal, closeModal }) => {
  const [articles, setArticles] = useState([]);
  const [favorite, setFavorite] = useState(
    Array.from(new Set(localStorage.getItem("favorite")))
  );
  const [cart, setCart] = useState(
    Array.from(new Set(localStorage.getItem("cart")))
  );

  useEffect(() => localStorage.setItem("favorite", favorite), [favorite]);
  useEffect(() => localStorage.setItem("cart", cart), [cart]);

  const toggleFavorite = (key) => {
    if (favorite.includes(`${key}`)) {
      setFavorite(favorite.filter((index) => index !== `${key}`));
    } else setFavorite([...favorite, `${key}`]);
  };

  const toggleCart = (key) => {
    if (cart.includes(`${key}`)) {
      setCart(cart.filter((index) => index !== `${key}`));
    } else setCart([...cart, `${key}`]);
  };

  return (
    <Switch>
      <Redirect exact from="/" to="/articles" />
      <Route path="/articles">
        <Articles
          articles={articles}
          favorite={favorite}
          cart={cart}
          setArticles={setArticles}
          addToFavorite={toggleFavorite}
          openModal={openModal}
          closeModal={closeModal}
          toggleCart={toggleCart}
        />
      </Route>
      <Route exact path="/favorite">
        <Favorite
          articles={articles}
          favorite={favorite}
          cart={cart}
          setArticles={setArticles}
          addToFavorite={toggleFavorite}
          openModal={openModal}
          closeModal={closeModal}
          toggleCart={toggleCart}
        />
      </Route>
      <Route exact path="/cart">
        <Cart
          articles={articles}
          favorite={favorite}
          cart={cart}
          setArticles={setArticles}
          addToFavorite={toggleFavorite}
          openModal={openModal}
          closeModal={closeModal}
          toggleCart={toggleCart}
        />
      </Route>
    </Switch>
  );
};

export default AppRoutes;
