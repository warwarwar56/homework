import "./Navigation.scss";
import { NavLink } from "react-router-dom";

const Navigation = () => {
  return (
    <ul className="nav">
      <li className="nav__link">
        <NavLink exact to="/articles" activeClassName="nav__link--active">
          Все карты
        </NavLink>
      </li>
      <li className="nav__link">
        <NavLink exact to="/favorite" activeClassName="nav__link--active">
          Избранные
        </NavLink>
      </li>
      <li className="nav__link">
        <NavLink exact to="/cart" activeClassName="nav__link--active">
          Корзина
        </NavLink>
      </li>
    </ul>
  );
};

export default Navigation;
