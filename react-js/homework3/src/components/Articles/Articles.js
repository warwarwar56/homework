import React, { useEffect } from "react";
import axios from "axios";
import Article from "../Article/Article";
import "./Articles.scss";

const Articles = ({
  articles,
  setArticles,
  addToFavorite,
  favorite,
  openModal,
  closeModal,
  toggleCart,
  cart,
}) => {
  useEffect(() => {
    axios("/articles.json").then((fetchedArticles) =>
      setArticles(fetchedArticles.data)
    );
  }, [setArticles]);

  const arrayOfArticles = articles.map((article, index) => (
    <Article
      article={article}
      key={index}
      index={index}
      addToFavorite={addToFavorite}
      favorite={favorite}
      openModal={openModal}
      closeModal={closeModal}
      toggleCart={toggleCart}
      cart={cart}
    />
  ));

  return <div className="allArticles">{arrayOfArticles}</div>;
};

export default Articles;
