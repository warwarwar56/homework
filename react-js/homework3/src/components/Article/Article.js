import React from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import Star from "../Star/Star";
import "./Article.scss";

const Article = ({
  article,
  index,
  addToFavorite,
  favorite,
  openModal,
  closeModal,
  toggleCart,
  cart,
}) => {
  const { name, price, image, gameClass } = article;
  const isFavorite = favorite.includes(`${index}`);
  const isInCart = cart.includes(`${index}`);
  const cartActions = [
    <Button text="закрыть" onClick={closeModal} />,
    <Button
      text={isInCart ? "удалить из корзины" : "добавить в корзину"}
      onClick={() => {
        toggleCart(index);
        closeModal();
      }}
    />,
  ];

  return (
    <div className="article">
      <p className="article__name">
        {name}{" "}
        <Star
          color="orangered"
          isFavorite={isFavorite}
          onClick={() => addToFavorite(index)}
        />
      </p>
      <img className="article__card" src={image} alt="card" />
      <p className="article__game-class">Класс: {gameClass}</p>
      <p className="article__price">
        Цена: {price}{" "}
        <img
          className="article__price-icon"
          alt="arcane dust"
          src="/imgs/arcaneDust.png"
        />
      </p>
      <Button
        text={isInCart ? "удалить из корзины" : "добавить в корзину"}
        onClick={() =>
          openModal(
            <Modal
              header="Корзина"
              text="вы точно хотите добавить карту в корзину ?"
              closeModal={closeModal}
              actions={cartActions}
            />
          )
        }
      />
    </div>
  );
};

export default Article;
