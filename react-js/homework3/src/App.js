import React, { useState } from "react";
import "./App.css";
import AppRoutes from "./routes/AppRoutes";
import Navigation from "./components/Navigation/Navigation";

const App = () => {
  const [modal, setModal] = useState(null);

  const openModal = (modal) => {
    setModal(modal);
  };

  const closeModal = () => {
    setModal(null);
  };

  return (
    <div className="App">
      <Navigation />
      <AppRoutes openModal={openModal} closeModal={closeModal} />
      {modal}
    </div>
  );
};

export default App;
