import React from 'react';
import './App.css';
import Button from './components/Button/Button'
import Modal from './components/Modal/Modal'

class App extends React.Component {
  state = {
    modal:null,
  }

  openModal = (modal) => {
    this.setState({modal});
  }

  closeModal = () => {
    this.setState({modal: null})
  }


 render() {
   return (
    <div className="App">
      <Button background='red'
              text='open first modal'
              onClick={() => this.openModal(<Modal header='First Modal' text='this is the first modal' closeModal={this.closeModal} actions={<Button background='blue' text='close' onClick={this.closeModal}/>} />)}
              />
      <Button background='green'
              text='open second modal'
              onClick={() => this.openModal(<Modal header='Second Modal' closeButton='true' closeModal={this.closeModal} text='this is the second modal' actions={<Button background='blue' text='close' onClick={this.closeModal}/>}/>)}
              />
    {this.state.modal}
    </div>
  );
} 
}

export default App;
