import React from 'react'


class Button extends React.Component {

    render() {
        const {background, text, onClick} = this.props;
        const style = {
            backgroundColor: background,
            padding: "10px",
            fontWeight: 'bold'
        }





        return (
            <button onClick={onClick} style={style}>{text}</button>
        )
    }
}


export default Button;