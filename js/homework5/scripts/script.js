let createNewUser = function () {
    let firstName = prompt("Name");
    let lastName = prompt("lastName");
    let birthday = prompt("Write your birthday dd.mm.yyyy: ");
    let newUser={
        firstName,
        lastName,
        birthday,
        getLogin: function() {
            return firstName[0].toLowerCase() + lastName.toLowerCase();
        },
        getAge: function() {
            birthday = new Date(birthday);
            let date = new Date();
            date = Math.ceil((((date.getTime() - birthday.getTime())/86400000)/365));
            return date;
        },
        getPassword: function() {
            return firstName[0].toUpperCase() + lastName.toLowerCase() + birthday.substring(6,10);
        }
    }
    Object.defineProperty(newUser, "firstName", {set : function(value) {
        this.firstName = value;
    }});
    Object.defineProperty(newUser, "lastName", {set : function(value){
        this.lastName = value;
    }});
    return newUser;
}

console.log(createNewUser().getPassword());

